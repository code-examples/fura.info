# [fura.info](https://fura.info) source codes

<br/>

### Run fura.info on localhost

    # vi /etc/systemd/system/fura.info.service

Insert code from fura.info.service

    # systemctl enable fura.info.service
    # systemctl start fura.info.service
    # systemctl status fura.info.service

http://localhost:4055
